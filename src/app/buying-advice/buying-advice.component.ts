import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import * as ti1 from 'technicalindicators';
import * as $ from 'jquery';
import * as ccxt from 'ccxt';
import * as cc from 'cryptocompare';
import { Exchange } from 'ccxt';
import * as mnt from 'moment';

@Component({
  selector: 'app-buying-advice',
  templateUrl: './buying-advice.component.html',
  styleUrls: ['./buying-advice.component.css']
})
export class BuyingAdviceComponent implements OnInit {

  autonio_token;
  advices = [];
  blurCss = { 'filter': 'blur(0px)' };
  constructor(public router: Router) { }

  ngOnInit() {
    if (localStorage.autonio_login_token != null) {
      this.autonio_token = JSON.parse(localStorage.getItem('autonio_login_token'));
    }
    else {
      this.autonio_token = null;
      this.router.navigateByUrl('/login');
    }

    $.ajax({
      url : 'https://autonio.foundation/webservices/get_buy_advices_user.php',
      method : 'POST',
      data : {'username' : this.autonio_token.username},
      success : (data)=>{
        if(data=='200'){
          this.electron_er('There is no data.');
          this.router.navigateByUrl('/advice');
        }
        else if(data!='404'){
          this.advices = data;
        }
        else{
          this.electron_er('There is some error, Please try again later.');
        }
      }
    });
  }

  electron_er(msg) {
    // electron.remote.showErrorBox('Error', msg);
    alert(msg);
  }

}
