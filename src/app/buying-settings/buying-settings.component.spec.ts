import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyingSettingsComponent } from './buying-settings.component';

describe('BuyingSettingsComponent', () => {
  let component: BuyingSettingsComponent;
  let fixture: ComponentFixture<BuyingSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyingSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyingSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
