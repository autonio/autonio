import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgStyle } from '@angular/common';

import * as ti1 from 'technicalindicators';
import * as $ from 'jquery';
import * as ccxt from 'ccxt';
import * as cc from 'cryptocompare';
import { Exchange } from 'ccxt';
import * as mnt from 'moment';

@Component({
  selector: 'app-advice',
  templateUrl: './advice.component.html',
  styleUrls: ['./advice.component.css']
})
export class AdviceComponent implements OnInit {

  autonio_token;
  advices;

  modal_open_buy: boolean = false;
  blurCss = { 'filter': 'blur(0px)' };

  seller_amount;
  seller_address;
  s_id;

  constructor(public router: Router) { }

  ngOnInit() {
    if (localStorage.autonio_login_token != null) {
      this.autonio_token = JSON.parse(localStorage.getItem('autonio_login_token'));
    }
    else {
      this.autonio_token = null;
      this.router.navigateByUrl('/login');
    }

    $.ajax({
      url: 'https://autonio.foundation/webservices/get_sell_advices.php',
      method: 'POST',
      success: (advices) => {
        if (advices == 200 && advices == 404) {

        }
        else {
          this.advices = advices;
        }
      }
    });
  }

  electron_er(msg) {
    // electron.remote.showErrorBox('Error', msg);
    alert(msg);
  }

  PerCalc(a, b) {

    return parseFloat(((a / 100) * b).toFixed(8));
  }

  ModalClose() {
    this.modal_open_buy = false;
    this.checkBlur();
  }

  checkBlur() {
    let style;
    if (this.modal_open_buy) {
      this.blurCss = { 'filter': 'blur(5px)' };
    }
    else {
      this.blurCss = { 'filter': 'blur(0px)' };
    }
    return style;
  }

  formatDate(date) {
    return mnt(date).format('YYYY-MM-DD HH:mm');
  }

  BuyAdviceModal(id, seller, price_usd) {
    let d = {
      'username': this.autonio_token.username,
      's_id': id
    };
    $.ajax({
      url: 'https://autonio.foundation/webservices/user_check_advice.php',
      method: 'POST',
      data: d,
      success: (data) => {
        data = parseInt(data);

        if (data == 200) {
          this.electron_er('You already bought this Advice , Please check it in buying section.');
        }
        else if (data == 400) {
          this.modal_open_buy = true;
          this.checkBlur();
          this.seller_amount = 0;
          this.seller_address = seller;
          this.s_id = id;

          $.ajax({
            url: 'https://min-api.cryptocompare.com/data/histoday?fsym=NIO*&tsym=USD&limit=2&aggregate=1&e=CCCAGG',
            datatype: 'json',
            success: (data) => {
              let Data = data['Data'];
              let close = Data[Data.length - 2]['close'];
              this.seller_amount = Math.round((price_usd / close));
            }
          });
        }
      }
    });
  }

  CheckPayment() {
    let gmt;
    $.ajax({
      url: 'http://api.timezonedb.com/v2/get-time-zone?key=WM7ACYIY2M17&format=json&by=zone&zone=Europe/London',
      success: (data) => {
        gmt = data['timestamp'];
        let gmt_date: any = new Date(gmt * 1000);

        $.ajax({
          url: 'https://api.ethplorer.io/getAddressHistory/' + this.seller_address + '?apiKey=freekey&type=transfer',
          success: (data) => {
            data = data.operations;
            let compare_date;
            let error = 1;
            let check_amount = this.seller_amount * 0.8;
            let received_amnt;
            let final_received;

            $.each(data, (i, val) => {
              compare_date = new Date(val.timestamp * 1000);
              var diffMs = gmt_date - compare_date;
              var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000);
              var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
              var diffDays = Math.floor(diffMs / 86400000); // days
              received_amnt = parseInt(val.value);

              if (diffMins <= 30 && diffHrs == 1 && diffDays == 0 && received_amnt >= check_amount && val.from == this.autonio_token.username.toLowerCase()) {
                final_received = received_amnt;
                error = 0;
              }
            });

            if (error == 0) {
              // console.log('Success');
              // Payment Confirmed
              let buy_data = {
                's_id': this.s_id,
                'username': this.autonio_token.username,
                'amount': final_received
              };
              $.ajax({
                url: 'https://autonio.foundation/webservices/user_buy_advice.php',
                method: 'POST',
                data: buy_data,
                success: (data1) => {
                  data1 = parseInt(data1);
                  switch (data1) {
                    case 400:
                      // console.log('Success');
                      this.electron_er('Success, Now you can see advisor\'s Details in "BUYING" Section.');
                      this.ModalClose();
                      break;

                    case 404:
                      this.electron_er('Some Error Occured, Please try again later');
                      this.ModalClose();
                      break;
                  }
                }
              });
            }
            else {
              this.electron_er('There is some error, Possibly you sent wrong amount or you sent from another address than your registered address.');
            }
          }
        });
      }
    });
    // https://api.ethplorer.io/getAddressHistory/0xff44d051383b08e1f56fcbc0cf1e4684bc6d2196?apiKey=freekey&token=0x5554e04e76533e1d14c52f05beef6c9d329e1e30&type=transfer

  }

}
