const electron = require('electron');
const url = require('url');
const path = require('path');
const ipc = electron.ipcMain;
var dialog = electron.dialog;
var fs = require("fs");

ELECTRON_ENABLE_STACK_DUMPING = true
ELECTRON_ENABLE_LOGGING = true

const isOnline = require('is-online');


process.env.NODE_ENV = 'production';

process.on('uncaughtException', function (err) {
    console.log(err);
});

process.on('unhandledRejection', function (err) {
    console.log(err);
});

const { app, BrowserWindow , Menu} = electron;
require('electron-debug')();

let mainWindow;


app.on('ready', function () {
    mainWindow = new BrowserWindow({
        'minHeight': 600,
        'minWidth': 1000,
        'frame': true,
        'toolbar': false,
        'show': false,
        'backgroundColor': '#112128',
        'icon': path.join(__dirname, './src/assets/img/logo.png')


    });
    // mainWindow.setMenu(null);

    // mainWindow.webContents.openDevTools({detach:true});

    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'dist/index.html'),
        protocol: 'file:',
        slashes: true,
        show: false
    }));
    mainWindow.once('ready-to-show', () => {
        mainWindow.show();
        mainWindow.maximize();
    });
    mainWindow.on('closed', function () {
        mainWindow = null;
    });


    var template = [{
        label: "Edit",
        submenu: [
            { label: "Cut", accelerator: "CmdOrCtrl+X", selector: "cut:" },
            { label: "Copy", accelerator: "CmdOrCtrl+C", selector: "copy:" },
            { label: "Paste", accelerator: "CmdOrCtrl+V", selector: "paste:" }
        ]
    }
    ];

    Menu.setApplicationMenu(Menu.buildFromTemplate(template));

});
app.on('window-all-closed', () => {
    if (process.platform !== 'darvin') {
        app.quit();
    }
});

ipc.on('redirect', function (e, url_1) {
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, url_1),
        protocol: 'file:',
        slashes: true,
        show: false
    }));
    mainWindow.once('ready-to-show', () => {
        mainWindow.show()
    });
});

ipc.on('error', function (e, msg) {
    dialog.showErrorBox('Error', msg);
});
