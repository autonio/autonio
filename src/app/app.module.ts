import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule} from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatNativeDateModule } from '@angular/material/core';
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SelectDropDownModule } from 'ngx-select-dropdown'; //for drop-box

import { AppComponent } from './app.component';
import { LiveComponent } from './components/live/live.component';
import { BacktestComponent } from './components/backtest/backtest.component';
import { AdviceComponent } from './components/advice/advice.component';
import { MarketComponent } from './components/market/market.component';
import { GeneticComponent } from './components/genetic/genetic.component';
import { SettingsComponent } from './components/settings/settings.component';
import { BalanceComponent } from './components/balance/balance.component';
import { AlgorithmComponent } from './components/myalgorithm/algo.component';


import { WebService } from './services/web.service';
import { TradingComponent } from './trading/trading.component';
import { LoginComponent } from './components/login/login.component';
import { MembershipComponent } from './components/membership/membership.component';
import { BuyingSettingsComponent } from './buying-settings/buying-settings.component';
import { SellingSettingsComponent } from './selling-settings/selling-settings.component';
import { BuyingAdviceComponent } from './buying-advice/buying-advice.component';
import { SellingAdviceComponent } from './selling-advice/selling-advice.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http,'./assets/i18n/', '.json');
}

const appRoutes: Routes = [
  { path: '', component: BacktestComponent },
  //{ path: 'backtest', component: BacktestComponent },
  { path: 'live', component: LiveComponent },
  { path: 'advice', component: AdviceComponent },
  { path: 'market', component: MarketComponent },
  { path: 'genetic', component: GeneticComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'login', component: LoginComponent },
  { path: 'membership', component: MembershipComponent },
  { path: 'buying_settings', component: BuyingSettingsComponent },
  { path: 'selling_settings', component: SellingSettingsComponent },
  { path: 'buying_advice', component: BuyingAdviceComponent },
  { path: 'selling_advice', component: SellingAdviceComponent },
  { path: 'balance', component: BalanceComponent },
  { path: 'saved_algorithm', component: AlgorithmComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    LiveComponent,
    BacktestComponent,
    AdviceComponent,
    MarketComponent,
    GeneticComponent,
    SettingsComponent,
    TradingComponent,
    LoginComponent,
    MembershipComponent,
    BuyingSettingsComponent,
    SellingSettingsComponent,
    BuyingAdviceComponent,
    SellingAdviceComponent,
    BalanceComponent,
    AlgorithmComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ReactiveFormsModule,
    FormsModule,
    SelectDropDownModule,
    MatDatepickerModule,
    // BrowserAnimationsModule,
    MatNativeDateModule,
    AmChartsModule
  ],
  exports: [
    BacktestComponent,
  ],
  providers: [WebService],
  bootstrap: [AppComponent]
})
export class AppModule { }
