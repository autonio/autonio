import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import * as ti1 from 'technicalindicators';
import * as $ from 'jquery';
import * as ccxt from 'ccxt';
import * as cc from 'cryptocompare';
import { Exchange } from 'ccxt';
import * as mnt from 'moment';

@Component({
  selector: 'app-selling-advice',
  templateUrl: './selling-advice.component.html',
  styleUrls: ['./selling-advice.component.css']
})
export class SellingAdviceComponent implements OnInit {

  autonio_token;
  advices = [];
  settings = [];
  blurCss = { 'filter': 'blur(0px)' };


  constructor(public router: Router) { }

  ngOnInit() {
    if (localStorage.autonio_login_token != null) {
      this.autonio_token = JSON.parse(localStorage.getItem('autonio_login_token'));
    }
    else {
      this.autonio_token = null;
      this.router.navigateByUrl('/login');
    }
    this.getAdvices();
  }

  getAdvices(){
    $.ajax({
      url: 'https://autonio.foundation/webservices/get_sell_advices_user.php',
      method: 'POST',
      data: { 'username': this.autonio_token.username },
      success: (data) => {
        this.advices = data;
      }
    });
  }

  electron_er(msg) {
    // electron.remote.showErrorBox('Error', msg);
    alert(msg);
  }

  PerCalc(a, b) {

    return parseFloat(((a / 100) * b).toFixed(8));
  }

  Delete(id) {
    console.log(id);
    $.ajax({
      url: 'https://autonio.foundation/webservices/delete_sell_advice.php',
      method: 'POST',
      data: { 'id': id },
      success: (data) => {
        data = parseInt(data);
        switch (data) {
          case 400:
            this.electron_er('Successfully Deleted');
            this.getAdvices();
            break;

          case 404:
            this.electron_er('Some Error Occured, Please try again later');
            break;
        }
      }
    });
  }

}
