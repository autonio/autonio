// import { BacktestComponent } from './../backtest/backtest.component';
// import { LiveComponent } from './../live/live.component';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { WebService } from './../../services/web.service';
import { ElementRef } from '@angular/core';

import * as ccxt from 'ccxt';
import * as ti1 from 'technicalindicators';
import * as cc from 'cryptocompare';
import * as $ from 'jquery';
@Component({
  selector: 'app-algo',
  templateUrl: './algo.component.html',
  styleUrls: ['./algo.component.css']
})
export class AlgorithmComponent implements OnInit {
  public href: string = "";
  indicators: string[];
  exchanges: string[];
  pairs: string[];
  ti = ti1['window'];
  session_dat;
  trade_dat;
  trade_dat_db;
  algorithm;
    // asset = 'BTC';
  // base = 'USD';
  constructor(private router: Router, private http: Http, private _elRef: ElementRef, private webservice: WebService, private cdRef:ChangeDetectorRef) { }

  ngOnInit() {
    this.getJSON("./assets/js/indicators.json").subscribe((data) => {
      this.indicators = (data);
    });
    // this.session_dat= JSON.parse((localStorage.getItem('autonio_session_token')));
    // console.log("Data from session=",this.session_dat.trade_data);
    // this.trade_dat=JSON.parse(this.session_dat.trade_data);
    var uname=JSON.parse(localStorage.autonio_login_token).username;
    let data={
      'uname':uname
    };
    //to load algorithm from backend
    $.ajax({
      method: 'POST',
      url: 'https://autonio.foundation/metamask_login/get_saved_algo.php',
      data: data,
      error: (data) => {
        console.log("error because of ",data);
      },
      success: (data) => {
        console.log("Data from back end=",JSON.parse(data));
        this.trade_dat_db=JSON.parse(data);
        this.algorithm=this.trade_dat_db.algorithm;
      }
    });
    // console.log("data from backtest is==",JSON.parse((localStorage.getItem('autonio_session_token'))));

  }
  public getJSON(url) {
    return this.http.get(url).map(res => res.json());
  }
  //trigger on click of backtest button from respective algorithm
  Backtest(val){
    // console.log("value on backtest click==",val);
    var dataBacktestForm = val;
    localStorage.setItem('tab_backtest_data', dataBacktestForm);
    $('.sidebar .links a').removeClass('active');
    $('.sidebar .links #backtest').addClass('active');
    this.router.navigateByUrl('/');
  }
  //trigger on click of live button from respective algorithm
  Live(val){
    // console.log("Value on live trade click==",val);
    var dataBacktestForm = val;
    localStorage.setItem('tab_backtest_data', dataBacktestForm);
    $('.sidebar .links a').removeClass('active');
    $('.sidebar .links #live').addClass('active');
    this.router.navigateByUrl('/live');
  }

}
