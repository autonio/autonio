import { WebService } from './../../services/web.service';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Http } from '@angular/http';
import { ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import { Router } from '@angular/router';

import * as ti1 from 'technicalindicators';
import * as $ from 'jquery';
import * as ccxt from 'ccxt';
import * as cc from 'cryptocompare';
import { Exchange } from 'ccxt';
import * as mnt from 'moment';

@Component({
  selector: 'app-backtest',
  templateUrl: './backtest.component.html',
  styleUrls: ['./backtest.component.css']
})
export class BacktestComponent implements OnInit {
  exchanges;
  pairs: string[];
  pari: string[];
  pair_detail: string[];
  indicators: string[];
  asset = 'BTC';
  asset_tv;
  base_tv;
  proxy = "https://cors-anywhere.herokuapp.com/";
  //asset='ETH';
  base = 'USD';
  ti = ti1['window'];
  final_buy_sell = [];
  maxDate = new Date();
  minDate;
  //for stop-loss and take profit
  lastsl = 0;
  lasttp = 0;
  slstate = 0;
  tpstate = 0;
  // export_data:string[];
  private chart: AmChart;
  chart_config;
  chartData;
  trade_count: number = 0;
  portfolio = 0;
  exchange_fees = 0;
  signal_inverse: boolean = false;
  modal_open_advice: boolean = false;
  blurCss = { 'filter': 'blur(0px)' };
  //for dropdown box.
  config = {
    displayKey: "", //if objects array passed which key to be displayed defaults to description
    search: true,
    placeholder:'--Select One--',
    height:'300px'
  };


  constructor(private router: Router,private webservice: WebService, private http: Http, private _elRef: ElementRef, private AmCharts: AmChartsService, private cdRef:ChangeDetectorRef) { }
  ngOnInit() {

    this.GenerateStartChart('bittrex');
    this.checkBlur();
    $(() => {
      $(".frm_inner_group .parameters").slideUp();
    });
    // to make parameters visible for users to make choice for further evaluation
    $(document).on("click", ".checkbox_group input[type='checkbox']", function () {
      if ($(this).prop('checked') == true) {
        $(this).parent().parent().parent().find('.parameters').slideDown();
      }
      else {
        $(this).parent().parent().parent().find('.parameters').slideUp();
      }
    });
  }
//To load chart for selected exchanges with predefined or user selected pairs.
  GenerateStartChart(exc) {
    let a, b;
    if (this.asset == 'USDT' && (exc != 'binance' && exc != 'kucoin' && exc != 'okex' && exc != 'kraken')) {
      a = 'USD';
    }
    else {
      if(exc=='ethfinex' && this.asset=='BTC'){
        this.asset='ETH';
      }
      a=this.asset;
    }
    if (this.base == 'USDT' && (exc != "binance" && exc != "kucoin" && exc != "okex" && exc !="karken")) {
      b = 'USD';
    }
    else {
      b = this.base;
    }
    let options = { aggregate: 1, timestamp: new Date(), exchange: exc };
    $('.chart_preloader').fadeIn(200);
    cc.histoMinute(a, b, options)
      .then(data => {
        let chartData = '[';
        $.each(data, (i, val) => {
          let timeStamp = new Date(val['time'] * 1000);
          let formated_date = this.formatDate(timeStamp.toString());
          chartData += '{"date" : "' + formated_date + '", "value" : ' + val['close'] + '}';
          if (i != data.length - 1) {
            chartData += ',';
          }
        });
        chartData += ']';
        let cd = JSON.parse(chartData);
        let chart_config = {
          "type": "serial",
          "theme": "dark",
          "addClassNames": true,
          "dataProvider": cd,
          "dataDateFormat": "YYYY-MM-DD HH:NN",
          "categoryField": "date",
          "creditsPosition": "bottom-right",
          "valueAxes": [{
            "axisAlpha": 0.2,
            "dashLength": 1,
            "position": "left"
          }],
          "defs": {
            "filter": {
              "id": "dropshadow",
              "x": "-10%",
              "y": "-10%",
              "width": "120%",
              "height": "120%",
              "feOffset": {
                "result": "offOut",
                "in": "rgba",
                "dx": "3",
                "dy": "3"
              },
              "feGaussianBlur": {
                "result": "blurOut",
                "in": "offOut",
                "stdDeviation": "5"
              },
              "feBlend": {
                "in": "SourceGraphic",
                "in2": "blurOut",
                "mode": "normal"
              }
            }
          },
          "graphs": [{
            "id": "g1",
            "type": "smoothedLine",
            "bulletSize": 28,
            "customBulletField": "customBullet",
            "balloonText": "[[value]]",
            "lineColor": "var(--blue-2)",
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "hideBulletsCount": 0,
            "valueField": "value",
            "useLineColorForBulletBorder": true,
            "lineThickness": 2
          }],
          "chartCursor": {
            "categoryBalloonDateFormat": "HH:NN, DD MMMM",
            "cursorPosition": "mouse"
          },
          "chartScrollbar": {
            "autoGridCount": true,
            "graph": "g1",
            "scrollbarHeight": 40
          },
          "categoryAxis": {
            "minPeriod": "mm",
            "parseDates": true,
            "axisColor": "#DADADA",
            "dashLength": 1,
            "minorGridEnabled": true
          }
        };
        this.chart = this.AmCharts.makeChart("chart", chart_config);
        $('.chart_preloader').fadeOut(200);
      })
      .catch(console.error);
  }
  //percentage calculation
  PerCalc(a, b) {
    return parseFloat(((a / 100) * b).toFixed(8));
  }

  formatDate(date) {
    return mnt(date).format('YYYY-MM-DD HH:mm');
  }

  public getJSON(url) {
    return this.http.get(url).map(res => res.json());
  }
  //trigger on change of indicators.
  IndIntChange(int, pair, exc) {
    let a, b;
    if (this.asset == 'USDT' && exc != "binance" && exc != "kucoin" && exc != "okex") {
      a = 'USD';
    }
    else {
      a = this.asset;
    }
    if (this.base == 'USDT' && exc != "binance" && exc != "kucoin" && exc != "okex") {
      b = 'USD';
    }
    else {
      b = this.base;
    }
    if (int < 60) {
      let options = { aggregate: int, exchange: exc };
      cc.histoMinute(a, b, options)
        .then(data => {
          this.minDate = new Date(data[0]['time'] * 1000);
        })
        .catch(console.error);
    }
    else if (int => 60 && int < 1440) {
      let options = { aggregate: int / 60, exchange: exc};
      cc.histoHour(a, b, options)
        .then(data => {
          this.minDate = new Date(data[0]['time'] * 1000);
        })
        .catch(console.error);
    }
    else {
      let options = { aggregate: int / 1440, exchange: exc };
      cc.histoDay(a, b, options)
        .then(data => {
          this.minDate = new Date(data[0]['time'] * 1000);
        })
        .catch(console.error);
    }
    //setting localStorage variable to track changes made by user.
    var tab_live_trade = '{ "tab_live_trade": 1 }';
    localStorage.setItem('tab_live_trade', tab_live_trade);
    $('.reset_test').css('opacity', '1').css('cursor', 'pointer').attr('disabled', false);
    $('.check_my_algo1').css('opacity', '1').css('cursor', 'pointer').attr('disabled', false);

  }
//trigger on change of pairs
  PairChange(exc, pair) {
    (async () => {
      let val1 = new ccxt[exc];
      //no need for build
      // val1.proxy=this.proxy;
      await val1.loadMarkets();
      let pair1 = val1.markets[pair];
      if(pair1['base']=='TZ'){
        this.asset='XTZ';
        this.asset_tv='XTZ';

      }
      else{
        this.asset = pair1['base'];
        this.asset_tv=pair1['base'];

      }
      this.base_tv=pair1['quote'];
      this.base = pair1['quote'];
      this.GenerateStartChart(exc);

    })()

    let a, b;
    if (this.asset == 'USDT' && exc != "binance" && exc != "kucoin" && exc != "okex" && exc !="karken") {
      a = 'USD';
    }
    else {
      a = this.asset;
    }
    if (this.base == 'USDT' && exc != "binance" && exc != "kucoin" && exc != "okex" && exc !="karken") {
      b = 'USD';
    }
    else {
      b = this.base;
    }
    let options = { aggregate: 1, exchange: exc };
    cc.histoMinute(a, b, options)
      .then(data => {
        this.minDate = new Date(data[0]['time'] * 1000);
      })
      .catch(console.error);

      var tab_live_trade = '{ "tab_live_trade": 1 }';
      localStorage.setItem('tab_live_trade', tab_live_trade);
      $('.reset_test').css('opacity', '1').css('cursor', 'pointer').attr('disabled', false);
      $('.check_my_algo1').css('opacity', '1').css('cursor', 'pointer').attr('disabled', false);

  }
  //triggers on pair search
  search(event){
    this.pairs=this.pari;  
    let result: string[] = [];
    for(let a of this.pairs){
      if(a.toLowerCase().indexOf(event) > -1){
        result.push(a)
      }
    }
    if(result.length>0){
      this.pairs=result;
      $("#pair").val(this.pairs[0]);
    }
    else{
      this.pairs=this.pari;
    }
    if(!event){
      this.pairs=this.pari;
    }
  }
//trigger in changes of exchanges
  ExchangeChange(exc) {
    (async () => {
        console.log("exchanges==",exc);
        if(exc=='ethfinex'){
          this.exchanges="BITFINEX";
        }
        else{
          this.exchanges=exc.toUpperCase();
        }
        let val1 = new ccxt[exc];
        //no need for build
        // val1.proxy=this.proxy;
        let pair_detail = await val1.load_markets();
        this.pairs = await Object.keys(pair_detail);
        console.log("pairssss error trac bfore chart",this.pairs);
        this.pairs.sort();
        this.pari=this.pairs;
        this.GenerateStartChart(exc);

    })()


    var tab_live_trade = '{ "tab_live_trade": 1 }';
    localStorage.setItem('tab_live_trade', tab_live_trade);
    $('.reset_test').css('opacity', '1').css('cursor', 'pointer').attr('disabled', false);
  }

  ngAfterViewInit(){
    //to restore page to previous state.
   var pairval;
   var dataLiveForm = $.parseJSON(localStorage.tab_backtest_data);
   if (dataLiveForm.length != 0 && dataLiveForm.length>0) {
     for(var i = 0; i < dataLiveForm.length; i++) {
       var singleCell = dataLiveForm[i];
       if(singleCell.name=="exchange") {
         $('[name='+singleCell.name+']', 'form').val(singleCell.value);
         $('[name='+singleCell.name+']', 'form').change();
         this.ExchangeChange(singleCell.value);
       } else if(singleCell.name=="pair") {
       } else {
         if($('[name='+singleCell.name+']', 'form').attr('type')=="checkbox") {
         } else {
           $('[name='+singleCell.name+']', 'form').val(singleCell.value);
         }
       }
     }
     var liveTabPair=setInterval(function () {
       if($('[name="pair"]', 'form').children('option').length>1){
         clearInterval(liveTabPair);
         if(dataLiveForm[1].name=="pair"){
           $('[name="pair"]', 'form').val(dataLiveForm[1].value);
           $('[name='+dataLiveForm[1].name+']', 'form').trigger("change");
           //this.PairChange($('[name="exchange"]', 'form').val(),dataLiveForm[1].value);
         }
       }
     }, 300);
     var liveTabInterval=setInterval(function () {
       if($('.toggler input[type="checkbox"]').length>10){
         clearInterval(liveTabInterval);
         for(var i = 0; i < dataLiveForm.length; i++) {
           var singleCell = dataLiveForm[i];
           if(singleCell.name=="exchange") {
           } else if(singleCell.name=="pair") {
           } else {
             if($('[name='+singleCell.name+']', 'form').attr('type')=="checkbox") {
               $('[name='+singleCell.name+']', 'form').trigger("click");
             } else {
               $('[name='+singleCell.name+']', 'form').val(singleCell.value);
             }
           }
         }
       }
     }, 300);
     if(dataLiveForm[1].name=="pair"){
       pairval= (dataLiveForm[1].value);
       var pos = pairval.indexOf("/");
       this.asset=pairval.slice(0,pos);
       this.base=pairval.slice(pos+1,pairval.length);
       this.cdRef.detectChanges();
     }
   }

 }
//alert method
  electron_er(msg) {
    alert(msg);
  }
  //trigger from PerformBacktest,plot buy sell signal in chart, calculate p/l,balances,
  MakeChart(timestamp, cp, start_bal, asset, base, start_asset, start_base, stop_loss, take_profit) {
    asset = parseFloat(asset);
    base = parseFloat(base);
    start_asset = parseFloat(start_asset);
    start_base = parseFloat(start_base);
    let chartData = '[';
    $.each(cp, (i, val) => {
      let timeStamp = new Date(timestamp[i] * 1000);
      let formated_date = this.formatDate(timeStamp.toString());
      if (this.final_buy_sell[i] == 'BUY') {
        this.lastsl = (val * ((100 - parseFloat(stop_loss)) / 100));
        let lasttp2 = (val * ((100 + parseFloat(take_profit)) / 100));
        this.slstate = 1;
        this.tpstate = 1;
        chartData += '{"date" : "' + formated_date + '", "value" : ' + val + ', "customBullet" : "assets/svg/buy.svg"}';
        this.trade_count++;
      }
      else if (this.final_buy_sell[i] == 'SELL') {
        chartData += '{"date" : "' + formated_date + '", "value" : ' + val + ', "customBullet" : "assets/svg/sell.svg"}';
        this.trade_count++;
      }
      else {
        chartData += '{"date" : "' + formated_date + '", "value" : ' + val + '}';
      }
      if (i < this.final_buy_sell.length - 1) {
        chartData += ',';
      }
    });
    chartData += ']';
    this.chartData = (JSON.parse(chartData));
    let end_bal: number = parseFloat(((asset * cp[cp.length - 1]) + base).toFixed(8));
    end_bal = parseFloat((end_bal - ((this.trade_count) * (this.portfolio) * (this.exchange_fees / 100))).toFixed(8));
    let profit_loss: any = (end_bal - start_bal).toFixed(8);
    let profit_loss_perc:any =(((end_bal - start_bal)/start_bal)*100).toFixed(2);
    let buy_hold: any = (start_asset * cp[cp.length - 1]) + start_base;
    buy_hold = buy_hold.toFixed(8);
    let profit_loss_buy_hold: any = (end_bal - buy_hold).toFixed(8);
    let profit_loss_buy_hold_perc:any=(((end_bal - buy_hold)/buy_hold)*100).toFixed(2);
    var base_c = this.base.toUpperCase();

    $('#chart').css('height', '100%');
    $('.result_preloader').fadeIn(200);

    let chart_config = {
      "type": "serial",
      "theme": "dark",
      "addClassNames": true,
      "dataProvider": this.chartData,
      "dataDateFormat": "YYYY-MM-DD HH:NN",
      "categoryField": "date",
      "creditsPosition": "bottom-right",
      "valueAxes": [{
        "axisAlpha": 0.2,
        "dashLength": 1,
        "position": "left"
      }],
      "defs": {
        "filter": {
          "id": "dropshadow",
          "x": "-10%",
          "y": "-10%",
          "width": "120%",
          "height": "120%",
          "feOffset": {
            "result": "offOut",
            "in": "rgba",
            "dx": "",
            "dy": ""
          },
          "feGaussianBlur": {
            "result": "",
            "in": "",
            "stdDeviation": ""
          },
          "feBlend": {
            "in": "SourceGraphic",
            "in2": "blurOut",
            "mode": "normal"
          }
        }
      },
      "graphs": [{
        "id": "g1",
        "type": "smoothedLine",
        "bulletSize": 14,
        "customBulletField": "customBullet",
        "balloonText": "[[value]]",
        "lineColor": "var(--blue-2)",
        "bulletBorderAlpha": 1,
        "bulletColor": "#FFFFFF",
        "hideBulletsCount": 0,
        "valueField": "value",
        "useLineColorForBulletBorder": true,
        "lineThickness": 2
      }],
      "chartCursor": {
        "categoryBalloonDateFormat": "HH:NN, DD MMMM",
        "cursorPosition": "mouse"
      },
      "chartScrollbar": {
        "autoGridCount": true,
        "graph": "g1",
        "scrollbarHeight": 40
      },
      "categoryAxis": {
        "minPeriod": "mm",
        "parseDates": true,
        "axisColor": "#DADADA",
        "dashLength": 1,
        "minorGridEnabled": true
      }
    };

    this.chart = this.AmCharts.makeChart("chart", chart_config);

    $('.chart_preloader').fadeOut(200);

    $('#start_bal .amount').html(start_bal + ' ' + base_c);
    $('#end_bal .amount').html(end_bal + ' ' + base_c);
    if (profit_loss < 0) {
      $('#profit_loss .amount').removeClass('green').addClass('red');
    }
    else {
      $('#profit_loss .amount').removeClass('red').addClass('green');
    }
    $('#profit_loss .amount').html(profit_loss + ' ' + base_c +'('+profit_loss_perc+'%)');

    if (profit_loss_buy_hold < 0) {
      $('#profit_loss_buy_hold .amount').removeClass('green').addClass('red');
    }
    else {
      $('#profit_loss_buy_hold .amount').removeClass('red').addClass('green');
    }
    $('#profit_loss_buy_hold .amount').html(profit_loss_buy_hold + ' ' + base_c+'('+profit_loss_buy_hold_perc+'%)');

    $('.result_preloader').fadeOut(200);
    $('.container .container-body .result .section img').fadeIn(200);


  }

  InverseToggle() {
    this.signal_inverse = !this.signal_inverse;
  }
  checkBlur() {
    let style;
    if (this.modal_open_advice) {
      this.blurCss = { 'filter': 'blur(5px)' };
    }
    else {
      this.blurCss = { 'filter': 'blur(0px)' };
    }
    return style;
  }
  myalgo(){
    this.router.navigateByUrl('/saved_algorithm');
  }
//to save selected user settings, which can be implement further for live trading.
  savealgo(name,cat){
    var indi=[];
    var catg,exc,pairs,domdat,uname,indi1;
        if(cat==1){
            catg=$('input[name="ccat"]').val();
        }
        else{
          catg=cat;
        }
        var items = [];
        $('.toggler input[type="checkbox"]').each(function () {
          if ($(this).prop('checked')) {
            var id = $(this).attr('id');
            items.push(id);
          }
        });
        if (items.length > 0) {
          $.each(this.indicators,function(i,val){
            $.each(items,function(j,val1){
              if(val1==val.name){
                indi.push(val.disp_name);
              }
            });
          });
        }
        else{
          this.electron_er('please select at least one indicators');
          this.modal_open_advice = false;
          this.checkBlur();
        }
        exc=$('select[name="exchange"]').val();
        pairs=$('select[name="pair"]').val();
        domdat=JSON.stringify($('form').serializeArray());
        uname= JSON.parse(localStorage.autonio_login_token).username;
        indi1=JSON.stringify(indi);
      if(name!=0 && catg !=0){
        let data = {
          'aname': name,
          'cata': catg,
          'exchange':exc,
          'pair':pairs,
          'indicators': indi1,
          'dom':domdat,
          'uname':uname
        };
        $.ajax({
          method: 'POST',
          url: 'https://autonio.foundation/metamask_login/save_algo.php',
          data: data,
          error: (data) => {
            this.electron_er('Some Error, Seems like your Internet Connection Issue. Please try again later');
          },
          success: (data) => {
            data = parseInt(data);
            switch (data) {
              case 400:
                this.electron_er("Algorithm saved succesfully");
                break;
              case 404:
                this.electron_er('Something Wrong happened, Please try again later');
                break;
            }
          }
        });
        this.modal_open_advice = false;
        this.checkBlur();
      }
      else {
        this.electron_er('Please enter information of algorithm.');
      }

  }
  //trigger in click of reset button to reset all selected settings
  Resest(){
   $('input[name="asset"]').val('1');
   $('input[name="base"]').val('500');
   $('select[name="exchange"]').val('');
   $('input[name="portfolio"]').val('100');
   $('input[name="startdate"]').val('');
   $('input[name="enddate"]').val('');
   this.pairs=[];
   $('.toggler input[type="checkbox"]').each(function () {
      $(this).prop('checked',false);
   });
   $('input[name="stop_loss"]').val('50');
   $('input[name="take_profit"]').val('50');
   $('select[name="ind_int"]').val(1);
   $('input[name="fees"]').val('0.25');
   var tab_live_trade = '{ "tab_live_trade": 0 }';
   localStorage.setItem('tab_live_trade', tab_live_trade);
   var dataBacktestForm = '{ "tab_backtest_data": 0 }';
   localStorage.setItem('tab_backtest_data', dataBacktestForm);
   $('.reset_test').attr('disabled', true).css('opacity', '0.3').css('cursor', 'not-allowed');
   $('.check_my_algo1').attr('disabled', true).css('opacity', '0.3').css('cursor', 'not-allowed');

 }
//trigger in click of backtest, to run backtest with selected settings.
  Backtest() {
    var pair, interval, after, before, period, exchange;
    $(document).ready(() => {
      function toTimestamp(strDate) {
        var datum = Date.parse(strDate);
        return datum / 1000;
      }
      let asset: number = parseFloat($('input[name="asset"]').val());
      let start_asset: number = asset;
      let base: number = parseFloat($('input[name="base"]').val());
      var portfolio_perc = parseFloat($('input[name="portfolio"]').val());
      var stop_loss = parseFloat($('input[name="stop_loss"]').val());
      var take_profit = parseFloat($('input[name="take_profit"]').val());
      let start_base: number = base;
      var startdate = $('input[name="startdate"]').val();
      var enddate = $('input[name="enddate"]').val();
      pair= $('ngx-select-dropdown[name="pair"]')[0].innerText;
      interval = $('select[name="ind_int"]').val();
      exchange = $('select[name="exchange"]').val();
      if (pair == '' || interval == '' || exchange == '' || stop_loss > 100 || take_profit > 100 || startdate == '' || enddate == '') {
        this.electron_er('Some required fields are missing like dates or exchange or pair');
      }
      else {
        var tab_live_trade = '{ "tab_live_trade": 1 }';
        localStorage.setItem('tab_live_trade', tab_live_trade);
        $('.back_test').css('opacity', '0.3').css('cursor', 'not-allowed').attr('disabled', true);
        setInterval(() => {
              $('.back_test').css('opacity', '1').css('cursor', 'pointer').attr('disabled', false);
        },10000);
        this.chartData = '';
        this.final_buy_sell = [];
        this.chart_config = '';
        this.portfolio = parseFloat(this.PerCalc(start_base, portfolio_perc).toFixed(8));
        this.exchange_fees = parseFloat($('input[name="fees"]').val());
        this.trade_count = 0;
        after = toTimestamp(startdate);
        before = toTimestamp(enddate);
        period = interval;
        this.GetBuySell(pair, interval, exchange, after, before, period, stop_loss, take_profit, asset, base, start_asset, start_base);
      }
    });
  }
//trigger from backtest method, collect selected parameters value and load marketdata for selected pairs.
  GetBuySell(pair, interval, exchange, after, before, period,stop_loss, take_profit, asset, base, start_asset, start_base) {
    $(() => {
      var items = [];
      $('.toggler input[type="checkbox"]').each(function () {
        if ($(this).prop('checked')) {
          var id = $(this).attr('id');
          items.push(id);
        }
      });
      var final_data = [];

      $.each(items, function (i, val) {
        var param_ar = [];
        var obj = {};
        $('.frm_inner_group.' + val + ' .parameters input').each(function () {
          var name = $(this).attr('name');
          var val1 = $(this).val();
          var obj1 = {};
          obj1[name] = val1;
          param_ar.push(obj1);
        });

        obj[val] = param_ar;
        final_data.push(obj);
      });

      if (final_data.length > 0) {

        var timestamp = [];
        var op = [];
        var hp = [];
        var lp = [];
        var cp = [];
        var volume = [];

        let a, b;
        if (this.asset == 'USDT' && (exchange != "binance") && (exchange != "kucoin") && (exchange != "okex")) {
          a = 'USD';
        }
        else {
          a = this.asset;
        }
        if (this.base == 'USDT' && (exchange != "binance") && (exchange != "kucoin") && (exchange != "okex")) {
          b = 'USD';
        }
        else {
          b = this.base;
        }

        if (period < 60) {
          let options = { aggregate: period, timestamp: new Date(before * 1000), exchange: exchange};
          cc.histoMinute(a, b, options)
            .then(data => {
              $.each(data, (i, val) => {
                if (val.time >= after) {
                  timestamp.push(val.time);
                  cp.push(val.close);
                  op.push(val.open);
                  lp.push(val.low);
                  hp.push(val.high);
                  if (i == data.length - 1) {
                    let avg = (data[i - 1]['volumefrom'] + data[i - 2]['volumefrom'] + data[i - 3]['volumefrom'] + data[i - 4]['volumefrom'] + data[i - 5]['volumefrom']) / 5;
                    volume.push(avg);
                  }
                  else {
                    volume.push(val.volumefrom);
                  }
                }
              });

              this.PerformBacktest(cp, timestamp, hp, op, lp, volume, final_data, asset, base, start_asset, start_base, stop_loss, take_profit);
            })
            .catch();
        }
        else if (period >= 60 && period < 1440) {
          let options = { aggregate: period / 60, timestamp: new Date(before * 1000), exchange: exchange};
          cc.histoHour(a, b, options)
            .then(data => {
              $.each(data, (i, val) => {
                if (val.time >= after) {
                  timestamp.push(val.time);
                  cp.push(val.close);
                  op.push(val.open);
                  lp.push(val.low);
                  hp.push(val.high);
                  if (i == data.length - 1) {
                    let avg = (data[i - 1]['volumefrom'] + data[i - 2]['volumefrom'] + data[i - 3]['volumefrom'] + data[i - 4]['volumefrom'] + data[i - 5]['volumefrom']) / 5;
                    volume.push(avg);
                  }
                  else {
                    volume.push(val.volumefrom);
                  }
                }
              });

              this.PerformBacktest(cp, timestamp, hp, op, lp, volume, final_data, asset, base, start_asset, start_base, stop_loss, take_profit);
            })
            .catch();
        }
        else {
          let options = { aggregate: period / 1440, timestamp: new Date(before * 1000), exchange: exchange};
          cc.histoDay(a, b, options)
            .then(data => {
              $.each(data, (i, val) => {
                if (val.time >= after) {
                  timestamp.push(val.time);
                  cp.push(val.close);
                  op.push(val.open);
                  lp.push(val.low);
                  hp.push(val.high);
                  if (i == data.length - 1) {
                    let avg = (data[i - 1]['volumefrom'] + data[i - 2]['volumefrom'] + data[i - 3]['volumefrom'] + data[i - 4]['volumefrom'] + data[i - 5]['volumefrom']) / 5;
                    volume.push(avg);
                  }
                  else {
                    volume.push(val.volumefrom);
                  }
                }
              });

              this.PerformBacktest(cp, timestamp, hp, op, lp, volume, final_data, asset, base, start_asset, start_base, stop_loss, take_profit);
            })
            .catch();
        }

      }
      else {
        this.electron_er('Please select at least one indicator');
      }
    });

  }
//trigger from getbuysell method,calculation of buy sell signals wrt selected indicators and loaded market data for selected exchanges and pairs
  PerformBacktest(cp, timestamp, hp, op, lp, volume, final_data, asset, base, start_asset, start_base, stop_loss, take_profit) {
    $('.container .actual_chart .chart_preloader').fadeIn(200);
    $('.result_preloader').css('display', 'flex');
    $('.result_preloader img').fadeIn(200);
    $('.container .container-body .result .section img').fadeOut(100);
    var indi_ar = [];
    var indi_name_ar = [];
    var indi_params = [];
    function array_pos_1(start, ar1, ar2) {
      var ar_return = [];
      for (var i = start, j = 0; i < ar2.length; i++ , j++) {
        if (j < start) {
          ar_return[j] = '';
        }
        ar_return[i] = ar1[j];
      }
      return ar_return;
    }
    var buy, sell;
    var input, tp;
    $.each(final_data, (i, val) => {
      var indi = (Object.keys(val)[0]);
      $.each(val, function (j, val2) {
        indi_params = val2;
      });
      switch (indi) {
        // case 'adl':
        //   input = { high: hp, low: lp, close: cp, volume: volume };
        //   var adl_res = this.ti.adl(input);
        //   adl_res=[1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1];
        //   console.log("return in backtest from adl indi=",adl_res);
        //   var adl_final = [];
        //   buy = 0;
        //   sell = 0;
        //   $.each(adl_res, function (i, val) {
        //     if (val>0 && buy == 0) {
        //       console.log("Buy signal");
        //       adl_final[i] = 'BUY';
        //       buy = 1;
        //       sell = 0;
        //     }
        //     else if (val<0 && sell == 0) {
        //       console.log("Sell signal");
        //       adl_final[i] = 'SELL';
        //       buy = 0;
        //       sell = 1;
        //     }
        //     else {
        //       adl_final[i] = '';
        //     }
        //   });
        //   indi_ar.push(adl_final);
        //   break;
        case 'adl':
          input = { high: hp, low: lp, close: cp, volume: volume };
          var adl_res = this.ti.adl(input);
          var adl_final = [];
          buy = 0;
          sell = 0;
          $.each(adl_res, function (i, val) {
            if (val >= 0 && buy == 0) {
              adl_final[i] = 'BUY';
              buy = 1;
              sell = 0;
            }
            else if (val < 0 && sell == 0) {
              adl_final[i] = 'SELL';
              buy = 0;
              sell = 1;
            }
            else {
              adl_final[i] = '';
            }
          });
          indi_ar.push(adl_final);
          break;
        case 'adx':
          tp = indi_params[0]['adx_time_period'];
          input = { period: tp, high: hp, low: lp, close: cp };
          var adx_final = [];
          var adx_res_temp = this.ti.adx(input);
          var adx_res_temp2 = [];
          buy = 0;
          sell = 0;
          $.each(adx_res_temp, function (i, val) {
            if (val['mdi'] >= val['pdi'] && buy == 0) {
              adx_res_temp2.push('BUY');
              buy = 1;
              sell = 0;
            }
            else if (val['mdi'] < val['pdi'] && sell == 0) {
              adx_res_temp2.push('SELL');
              buy = 0;
              sell = 1;
            }
            else {
              adx_res_temp2.push('');
            }
          });
          var starting_index = cp.length - adx_res_temp2.length;
          for (var i: any = starting_index, j = 0; i < cp.length; i++ , j++) {
            if (j < starting_index) {
              adx_final[j] = '';
            }
            adx_final[i] = adx_res_temp2[j];
          }
          indi_ar.push(adx_final);
          break;

        case 'atr':
          tp = indi_params[0]['atr_time_period'];
          input = { period: tp, high: hp, low: lp, close: cp };
          var atr_res_temp = this.ti.atr(input);
          var atr_res = [];
          var atr_final = [];
          starting_index = cp.length - atr_res_temp.length;
          for (var i: any = starting_index, j = 0; i < cp.length; i++ , j++) {
            if (j < starting_index) {
              atr_res[j] = '';
            }
            atr_res[i] = atr_res_temp[j];
          }
          buy = 0;
          sell = 0;
          $.each(atr_res, function (i, val) {
            if (val != '') {
              if (cp[i] >= val && buy == 0) {
                atr_final[i] = 'BUY';
                buy = 1;
                sell = 0;
              }
              else if (cp[i] < val && sell == 0) {
                atr_final[i] = 'SELL';
                buy = 0;
                sell = 1;
              }
              else {
                atr_final[i] = '';
              }
            }
            else {
              atr_final[i] = '';
            }
          });
          indi_ar.push(atr_final);
          break;

        case 'bollingerbands':
          tp = parseInt(indi_params[0]['bb_time_period']);
          var sd1 = parseInt(indi_params[1]['bb_stddev']);
          input = { period: tp, values: cp, stdDev: sd1 };
          var bollingerbands_res = this.ti.bollingerbands(input);
          var bb_lower = [];
          var bb_upper = [];
          var bollingerbands_final = [];
          var remain = cp.length - bollingerbands_res.length;
          j = remain;
          $.each(bollingerbands_res, function (i, val) {
            if (i < remain) {
              bb_lower[i] = '';
              bb_upper[i] = '';
            }
            bb_lower[j] = val['lower'];
            bb_upper[j] = val['upper'];
            j++;
          });
          buy = 0;
          sell = 0;
          $.each(cp, function (i, val) {
            if (val != '') {
              if (val < bb_lower[i] && buy == 0) {
                bollingerbands_final[i] = 'BUY';
                buy = 1;
                sell = 0;
              }
              else if (val > bb_upper[i] && sell == 0) {
                bollingerbands_final[i] = 'SELL';
                buy = 0;
                sell = 1;
              }
              else {
                bollingerbands_final[i] = '';
              }
            }
            else {
              bollingerbands_final[i] = '';
            }
          });
          indi_ar.push(bollingerbands_final);
          break;

        case 'cci':
          tp = parseInt(indi_params[0]['cci_time_period']);
          var ul = parseInt(indi_params[1]['cci_upper_limit']);
          var ll = parseInt(indi_params[2]['cci_lower_limit']);
          input = { period: tp, open: op, high: hp, low: lp, close: cp };
          var cci_res_temp = this.ti.cci(input);
          var cci_res = [];
          var cci_final = [];
          starting_index = cp.length - cci_res_temp.length;
          for (var i: any = starting_index, j = 0; i < cp.length; i++ , j++) {
            if (j < starting_index) {
              cci_res[j] = '';
            }
            cci_res[i] = cci_res_temp[j];
          }
          sell = 0;
          buy = 0;
          $.each(cci_res, function (i, val) {
            if (val != '') {
              if (val >= ul && buy == 0) {
                cci_final[i] = 'BUY';
                buy = 1;
                sell = 0;
              }
              else if (val < ll && sell == 0) {
                cci_final[i] = 'SELL';
                buy = 0;
                sell = 1;
              }
              else {
                cci_final[i] = '';
              }
            }
            else {
              cci_final[i] = '';
            }
          });
          indi_ar.push(cci_final);
          break;

        case 'forceindex':
          tp = parseInt(indi_params[0]['fi_time_period']);
          input = { period: tp, open: op, high: hp, low: lp, close: cp, volume: volume };
          var forceindex_res_temp = this.ti.forceindex(input);
          var forceindex_final = [];
          starting_index = cp.length - forceindex_res_temp.length;
          var forceindex_res = array_pos_1(starting_index, forceindex_res_temp, cp);
          buy = 0;
          sell = 0;
          $.each(forceindex_res, function (i, val) {
            if (val != '') {
              if (val >= 0 && buy == 0) {
                forceindex_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val < 0 && sell == 0) {
                forceindex_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                forceindex_final[i] = '';
              }
            }
            else {
              forceindex_final[i] = '';
            }
          });
          indi_ar.push(forceindex_final);
          break;

        case 'kst':
          tp = parseInt(indi_params[0]['kst_time_period']);
          var roc1 = parseInt(indi_params[1]['kst_roc1']);
          var roc2 = parseInt(indi_params[2]['kst_roc2']);
          var roc3 = parseInt(indi_params[3]['kst_roc3']);
          var roc4 = parseInt(indi_params[4]['kst_roc4']);
          var smroc1 = parseInt(indi_params[5]['kst_smroc1']);
          var smroc2 = parseInt(indi_params[6]['kst_smroc2']);
          var smroc3 = parseInt(indi_params[7]['kst_smroc3']);
          var smroc4 = parseInt(indi_params[8]['kst_smroc4']);
          input = {
            values: cp,
            ROCPer1: roc1,
            ROCPer2: roc2,
            ROCPer3: roc3,
            ROCPer4: roc4,
            SMAROCPer1: smroc1,
            SMAROCPer2: smroc2,
            SMAROCPer3: smroc3,
            SMAROCPer4: smroc4,
            signalPeriod: tp
          };
          var kst_res_temp = this.ti.kst(input);
          var kst_res_temp1 = [];
          $.each(kst_res_temp, function (i, val) {
            kst_res_temp1.push(val['kst']);
          });
          starting_index = cp.length - kst_res_temp1.length;
          var kst_res = array_pos_1(starting_index, kst_res_temp1, cp);
          var kst_final = [];
          buy = 0;
          sell = 0;
          $.each(kst_res, function (i, val) {
            if (val != '') {
              if (val >= 0 && buy == 0) {
                kst_final[i] = 'BUY'
                buy = 1; sell = 0;
              }
              else if (val < 0 && sell == 0) {
                kst_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                kst_final[i] = '';
              }
            }
            else {
              kst_final[i] = '';
            }
          });
          indi_ar.push(kst_final);
          break;

        case 'macd':
          var fp = parseInt(indi_params[0]['macd_fast_period']);
          var sp = parseInt(indi_params[1]['macd_slow_period']);
          var slp = parseInt(indi_params[2]['macd_signal_period']);
          input = {
            values: cp,
            fastPeriod: fp,
            slowPeriod: sp,
            signalPeriod: slp,
            SimpleMAOscillator: false,
            SimpleMASignal: false
          };
          var macd_res_temp = this.ti.macd(input);
          var macd_res_temp1 = [];
          var macd_final = [];
          $.each(macd_res_temp, function (i, val) {
            if (val['signal'] != undefined) {
              macd_res_temp1[i] = val['signal'];
            }
            else {
              macd_res_temp1[i] = '';
            }
          });
          starting_index = cp.length - macd_res_temp1.length;
          var macd_res = array_pos_1(starting_index, macd_res_temp1, cp);
          buy = 0;
          sell = 0;
          $.each(macd_res, function (i, val) {
            if (val != '') {
              if (val >= 0 && buy == 0) {
                macd_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val < 0 && sell == 0) {
                macd_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                macd_final[i] = '';
              }
            }
            else {
              macd_final[i] = '';
            }
          });
          indi_ar.push(macd_final);
          break;

        case 'obv':
          input = { close: cp, volume: volume };
          var obv_res_temp = this.ti.obv(input);
          starting_index = cp.length - obv_res_temp.length;
          var obv_res = array_pos_1(starting_index, obv_res_temp, cp);
          buy = 0;
          sell = 0;
          var obv_final = [];
          $.each(obv_res, function (i, val) {
            if (val != '') {
              if (val >= 0 && buy == 0) {
                obv_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val < 0 && sell == 0) {
                obv_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                obv_final[i] = '';
              }
            }
            else {
              obv_final[i] = '';
            }
          });
          indi_ar.push(obv_final);
          break;

        case 'psar':
          var step = parseInt(indi_params[0]['psar_steps']);
          var max = parseInt(indi_params[1]['psar_max']);
          input = { high: hp, low: lp, step: step, max: max };
          var psar_res_temp = this.ti.psar(input);
          var psar_final = [];
          buy = 0;
          sell = 0;
          $.each(psar_res_temp, function (i, val) {
            if (cp[i] >= val && buy == 0) {
              psar_final[i] = 'BUY';
              buy = 1; sell = 0;
            }
            else if (cp[i] < val && sell == 0) {
              psar_final[i] = 'SELL';
              buy = 0; sell = 1;
            }
            else {
              psar_final[i] = '';
            }
          });
          indi_ar.push(psar_final);
          break;

        case 'roc':
          tp = parseInt(indi_params[0]['roc_time_period']);
          input = { period: tp, values: cp };
          var roc_res_temp = this.ti.roc(input);
          starting_index = cp.length - roc_res_temp.length;
          var roc_res = array_pos_1(starting_index, roc_res_temp, cp);
          var roc_final = [];
          buy = 0;
          sell = 0;
          $.each(roc_res, function (i, val) {
            if (val != '') {
              if (val >= 0 && buy == 0) {
                roc_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val < 0 && sell == 0) {
                roc_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                roc_final[i] = '';
              }
            }
            else {
              roc_final[i] = '';
            }
          });
          indi_ar.push(roc_final);
          break;

        case 'rsi':
          tp = parseInt(indi_params[0]['rsi_time_period']);
          var ul = parseInt(indi_params[1]['rsi_upper_limit']);
          var ll = parseInt(indi_params[2]['rsi_lower_limit']);

          input = { period: tp, values: cp };
          var rsi_res_temp = this.ti.rsi(input);
          starting_index = cp.length - rsi_res_temp.length;
          var rsi_res = array_pos_1(starting_index, rsi_res_temp, cp);
          var rsi_final = [];
          buy = 0;
          sell = 0;
          $.each(rsi_res, function (i, val) {
            if (val != '') {
              if (val <= ll && buy == 0) {
                rsi_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val >= ul && sell == 0) {
                rsi_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                rsi_final[i] = '';
              }
            }
            else {
              rsi_final[i] = '';
            }
          });
          indi_ar.push(rsi_final);
          break;

        case 'sma':
          var short = parseInt(indi_params[0]['sma_short_period']);
          var input_short = { period: short, values: cp };
          var long = parseInt(indi_params[1]['sma_long_period']);
          var input_long = { period: long, values: cp };
          var sma_short_temp = this.ti.sma(input_short);
          var sma_long_temp = this.ti.sma(input_long);
          var starting_index_short = cp.length - sma_short_temp.length;
          var sma_short_temp1 = array_pos_1(starting_index_short, sma_short_temp, cp);
          var starting_index_long = cp.length - sma_long_temp.length;
          var sma_long_temp1 = array_pos_1(starting_index_long, sma_long_temp, cp);
          var sma_final = [];
          buy = 0;
          sell = 0;
          $.each(cp, function (i, val) {
            if (sma_short_temp1[i] != '' && sma_long_temp1[i] != '') {
              if (sma_short_temp1[i] >= sma_long_temp1[i] && buy == 0) {
                sma_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (sma_short_temp1[i] < sma_long_temp1[i] && sell == 0) {
                sma_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                sma_final[i] = '';
              }
            }
            else {
              sma_final[i] = '';
            }
          });
          indi_ar.push(sma_final);
          break;

        case 'ema':
          short = parseInt(indi_params[0]['ema_short_period']);
          input_short = { period: short, values: cp };
          long = parseInt(indi_params[1]['ema_long_period']);
          input_long = { period: long, values: cp };
          var ema_short_temp = this.ti.ema(input_short);
          var ema_long_temp = this.ti.ema(input_long);
          starting_index_short = cp.length - ema_short_temp.length;
          var ema_short_temp1 = array_pos_1(starting_index_short, ema_short_temp, cp);
          starting_index_long = cp.length - ema_long_temp.length;
          var ema_long_temp1 = array_pos_1(starting_index_long, ema_long_temp, cp);
          var ema_final = [];
          buy = 0;
          sell = 0;
          $.each(cp, function (i, val) {
            if (ema_short_temp1[i] != '' && ema_long_temp1[i] != '') {
              if (ema_short_temp1[i] >= ema_long_temp1[i] && buy == 0) {
                ema_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (ema_short_temp1[i] < ema_long_temp1[i] && sell == 0) {
                ema_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                ema_final[i] = '';
              }
            }
            else {
              ema_final[i] = '';
            }
          });
          indi_ar.push(ema_final);
          break;

        case 'stochastic':
          tp = parseInt(indi_params[0]['kd_time_period']);
          sp = parseInt(indi_params[1]['kd_signal_period']);
          input = { high: hp, low: lp, close: cp, period: tp, signalPeriod: sp };
          var stochastic_res_temp = this.ti.stochastic(input);
          var stochastic_final = [];
          var stochastic_k = [];
          var stochastic_d = [];
          remain = cp.length - stochastic_res_temp.length;
          j = remain;
          buy = 0;
          sell = 0;
          $.each(stochastic_res_temp, function (i, val) {
            if (val['k'] != undefined && val['d'] != undefined) {
              stochastic_k.push(val['k']);
              stochastic_d.push(val['d']);
            }
            else {
              stochastic_k.push('');
              stochastic_d.push('');
            }
          });
          var starting_index_k = cp.length - stochastic_k.length;
          var stochastic_k1 = array_pos_1(starting_index_k, stochastic_k, cp);
          var starting_index_d = cp.length - stochastic_d.length;
          var stochastic_d1 = array_pos_1(starting_index_d, stochastic_d, cp);
          $.each(cp, function (i, val) {
            if (stochastic_k1[i] != '' && stochastic_d1[i] != '') {
              if (stochastic_d1[i] >= stochastic_k1[i] && buy == 0) {
                stochastic_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (stochastic_d1[i] < stochastic_k1[i] && sell == 0) {
                stochastic_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                stochastic_final[i] = '';
              }
            }
            else {
              stochastic_final[i] = '';
            }
          });
          indi_ar.push(stochastic_final);
          break;

        case 'trix':
          tp = parseInt(indi_params[0]['trix_time_period']);
          input = { values: cp, period: tp };
          var trix_res_temp = this.ti.trix(input);
          starting_index = cp.length - trix_res_temp.length;
          var trix_res = array_pos_1(starting_index, trix_res_temp, cp);
          var trix_final = [];
          buy = 0; sell = 0;
          $.each(trix_res, function (i, val) {
            if (val != '') {
              if (val >= 0 && buy == 0) {
                trix_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val < 0 && sell == 0) {
                trix_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                trix_final[i] = '';
              }
            }
            else {
              trix_final[i] = '';
            }
          });
          indi_ar.push(trix_final);
          break;

        case 'vwap':
          input = {
            open: op,
            high: hp,
            low: lp,
            close: cp,
            volume: volume
          };
          var vwap_res_temp = this.ti.vwap(input);
          var vwap_final = [];
          buy = 0; sell = 0;
          $.each(vwap_res_temp, function (i, val) {
            if (val >= cp[i] && buy == 0) {
              vwap_final[i] = 'BUY';
              buy = 1; sell = 0;
            }
            else if (val < cp[i] && sell == 0) {
              vwap_final[i] = 'SELL';
              buy = 0; sell = 1;
            }
            else {
              vwap_final[i] = '';
            }
          });
          indi_ar.push(vwap_final);
          break;

        case 'wma':
          tp = parseInt(indi_params[0]['wma_time_period']);
          input = { values: cp, period: tp };
          var wma_res_temp = this.ti.wma(input);
          starting_index = cp.length - wma_res_temp.length;
          var wma_res = array_pos_1(starting_index, wma_res_temp, cp);
          var wma_final = [];
          buy = 0; sell = 0;
          $.each(wma_res, function (i, val) {
            if (val != '') {
              if (val >= cp[i] && buy == 0) {
                wma_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val < cp[i] && sell == 0) {
                wma_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                wma_final[i] = '';
              }
            }
            else {
              wma_final[i] = '';
            }
          });
          indi_ar.push(wma_final);
          break;

        case 'wema':
          tp = parseInt(indi_params[0]['wema_time_period']);
          input = { values: cp, period: tp };
          var wema_res_temp = this.ti.wema(input);
          starting_index = cp.length - wema_res_temp.length;
          var wema_res = array_pos_1(starting_index, wema_res_temp, cp);
          var wema_final = [];
          buy = 0; sell = 0;
          $.each(wema_res, function (i, val) {
            if (val != '') {
              if (cp[i] >= val && buy == 0) {
                wema_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (cp[i] < val && sell == 0) {
                wema_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                wema_final[i] = '';
              }
            }
            else {
              wema_final[i] = '';
            }
          });
          indi_ar.push(wema_final);
          break;

        case 'williamsr':
          tp = parseInt(indi_params[0]['williamsr_time_period']);
          input = { high: hp, low: lp, close: cp, period: tp };
          var williamsr_res_temp = this.ti.williamsr(input);
          starting_index = cp.length - williamsr_res_temp.length;
          var williamsr_res = array_pos_1(starting_index, williamsr_res_temp, cp);
          var williamsr_final = [];
          buy = 0; sell = 0;
          $.each(williamsr_res, function (i, val) {
            if (val != '') {
              if (val >= 20 && buy == 0) {
                williamsr_final[i] = 'BUY';
                buy = 1; sell = 0;
              }
              else if (val < 20 && sell == 0) {
                williamsr_final[i] = 'SELL';
                buy = 0; sell = 1;
              }
              else {
                williamsr_final[i] = '';
              }
            }
            else {
              williamsr_final[i] = '';
            }
          });
          indi_ar.push(williamsr_final);
          break;
        default:
          break;
      }
      indi_name_ar.push(indi + '_final');
    });
    let sl_ar = [];
    $.each(cp, (i, val) => {
      if (this.lastsl > val && this.slstate == 1) {
        sl_ar[i] = 'SELL';
        this.tpstate = 0;
        this.slstate = 0;
      }
      else {
        sl_ar[i] = '';
      }
    });
    indi_ar.push(sl_ar);
    var hold_key_ar = [];
    var buy_key_ar = [];
    var sell_key_ar = [];
    $.each(cp, function (i, val) {
      buy_key_ar[i] = 0;
      sell_key_ar[i] = 0;
      hold_key_ar[i] = 0;
    });
    $.each(indi_ar, function (i, val) {
      $.each(val, function (j, val1) {
        if (val1 == 'SELL') {
          sell_key_ar[j] += 1;
        }
        else if (val1 == 'BUY') {
          buy_key_ar[j] += 1;
        }
        else {
          hold_key_ar[j] += 1;
        }
      });
    });
    buy = 0; sell = 0;

    let buy_sell = [];
    buy = 0; sell = 0;

    $.each(cp, (i, val) => {
      if (sell_key_ar[i] > buy_key_ar[i] && sell == 0) {
        if (this.signal_inverse) {
          buy_sell[i] = 'BUY';
        }
        else {
          buy_sell[i] = 'SELL';
        }
      }
      else if (sell_key_ar[i] < buy_key_ar[i] && buy == 0) {
        if (this.signal_inverse) {
          buy_sell[i] = 'SELL';
        }
        else {
          buy_sell[i] = 'BUY';
        }
      }
      else {
        buy_sell[i] = '';
      }
    });
    let start_bal = ((asset * cp[0]) + base).toFixed(8);

    $.each(buy_sell, (i, val) => {
      if (val == 'SELL' && sell == 0) {
        this.final_buy_sell[i] = 'SELL';
        base += parseFloat((cp[i] * asset).toFixed(8));
        asset = 0;
        sell = 1; buy = 0;
      }
      else if (val == 'BUY' && buy == 0) {
        this.final_buy_sell[i] = 'BUY';
        asset += parseFloat((this.portfolio / cp[i]).toFixed(8));
        base -= this.portfolio;
        sell = 0; buy = 1;
      }
      else {
        this.final_buy_sell[i] = '';
      }
    });

    this.MakeChart(timestamp, cp, start_bal, asset, base, start_asset, start_base,  stop_loss, take_profit);
  }


}
