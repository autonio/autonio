import { AppComponent } from './../../app.component';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  pass_ind = 0;
  pass_ind1 = 0;
  autonio_token;

  constructor(private router: Router) { }

  ngOnInit() {
    if (localStorage.autonio_login_token != null) {
      this.autonio_token = JSON.parse(localStorage.getItem('autonio_login_token'));
    }
    else {
      this.autonio_token = null;
      this.router.navigateByUrl('/login');
      //below code is required only if needed to implement signup features
      // document.getElementById('signup').style.display='none';
      // document.getElementById('signbtn2').style.display='none';

    }
  }

  electron_er(msg) {
    alert(msg);
  }
  reset_pass_ind1() {
    this.pass_ind1 = 0;

  }
  
  reset_pass_ind() {
    this.pass_ind = 0;

  }
  //if user is authenticate to get in
  LoginUser(username) {
    var login_token = `{
      "username":"` + username + `",
        "keys" : [
          { "name": "binance", "api": "", "secret": "" },
          { "name": "ethfinex", "api": "", "secret": "" },
          { "name": "bitfinex", "api": "", "secret": "" },
          { "name": "bitshares", "account": "", "api": "" },
          { "name": "deex", "account": "", "api": "" },
          { "name": "binance-dex", "account": ""},
          { "name": "bitstamp", "api": "", "secret": "" },
          { "name": "kucoin", "api": "", "secret": "" ,"password":""},
          { "name": "kucoin2", "api": "", "secret": "" ,"password":""},
          { "name": "kraken", "api": "", "secret": "" },
          { "name": "bittrex", "api": "", "secret": "" },
          { "name": "okex", "api": "", "secret": ""},
          { "name": "coss", "api": "", "secret": ""},
          { "name": "okex3", "api": "", "secret": ""}


        ]
      }`;
      var session_dat=`{
        "username":"` + username + `",
          "start_date":"",
          "information" : [],
          "trade_data":"",
          "trade_stat":[],
          "stop_date":""
        }`;
        var market_maker_session=`{
          "username":"` + username + `",
            "start_date":"",
            "market_stat_msg" : [],
            "maker_data":"",
            "start_bal":"",
            "end_bal":"",
            "stop_date":""
          }`;
    //initialization of differnet localStorage variables.
    localStorage.setItem('autonio_login_token', (login_token));
    var tab_live_trade = '{ "tab_live_trade": 0 }';
    localStorage.setItem('tab_live_trade', tab_live_trade);
    localStorage.setItem('autonio_session_token', (session_dat));
    localStorage.setItem('market_maker_session', (market_maker_session));

    this.autonio_token = JSON.parse(login_token);
    var dataBacktestForm = '{ "tab_backtest_data": 0 }';
    localStorage.setItem('tab_backtest_data', dataBacktestForm);
    //Navigate to initial page i.e Backtest
    this.router.navigateByUrl('/');
  }
//trigger on click of login button
  Login(username, pass) {
    let data = {
      'name': username,
      'pass': pass
    };
    $.ajax({
      method: 'POST',
      url: 'https://autonio.foundation/metamask_login/user_check_1.php',
      data: data,
      error: (data) => {
        this.electron_er('Some Error, Seems like your Internet Connection Issue. Please try again later');
      },
      success: (data) => {
        console.log("Data from back end=",data);
        data = parseInt(data);
        switch (data) {
          case 100:
            this.LoginUser(username);
            break;
          case 202:
            this.electron_er('Wrong Password');
            this.pass_ind = 1;
            break;
          case 101:
            this.electron_er('Invalid Username');
            this.pass_ind1 = 1;
            break;
          case 404:
            this.electron_er('Something Wrong happened, Please try again later');
            break;

        }

      }
    });
  }

}
