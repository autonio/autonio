import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  autonio_token;
  user_name;
  constructor(private router: Router) { }

  ngOnInit() {
    if (localStorage.autonio_login_token != null) {
      this.autonio_token = JSON.parse(localStorage.getItem('autonio_login_token'));
      let uname = JSON.parse(localStorage.autonio_login_token).username;
      this.user_name=uname;
    }
    else {
      this.autonio_token = null;
      this.router.navigateByUrl('/login');
    }
  }
 //trigger on changes in exchanges
  exchange_change(val) {
    console.log(val);
    $('.add_keys_form').slideDown();
    $('input[name="api"]').val('');
    $('input[name="secret"]').val('');
    $('input[name="pass1"]').val('');
    if (this.autonio_token.keys.length > 0) {
      $.each(this.autonio_token.keys, (i, val1) => {
        if (val1.name == val) {
          $('input[name="api"]').val(val1.api);
          $('input[name="secret"]').val(val1.secret);
          $('input[name="pass1"]').val(val1.password);
        }
      });
    }
    if(val=="okex" && !$('input[name="api"]').val()){
      alert("Please make sure you enter v1 API Keys, Thank You !");

    }


  }
  //for binance-Dex
  Addkeys11(){
      var exc= $('select[name="exchange"]').val();
      var api= $('input[name="api"]').val();
      if(!api){
        alert("Missing Field, Please try again");
      }
      else{
        if (this.autonio_token.keys.length > 0) {
          $.each(this.autonio_token.keys, (i, val1) => {
            if (val1.name == exc ) {
              val1.account = api.trim();
              alert('Key Updated Successfully');
            }
          });
       }
       //keys store in localStorage
        localStorage.setItem('autonio_login_token', JSON.stringify(this.autonio_token));

      }
  }
//  for kucoin
  Addkeys1(){
    var exc= $('select[name="exchange"]').val();
    var api= $('input[name="api"]').val();
    var sec= $('input[name="secret"]').val();
    var pass=$('input[name="pass1"]').val();
    if(!pass || !sec || !api){
      alert("Some Missing Fields, Please try again");
    }
    else{
      if (this.autonio_token.keys.length > 0) {
        $.each(this.autonio_token.keys, (i, val1) => {
          if (val1.name == exc ) {
            val1.api = api.trim();
            val1.secret = sec.trim();
            val1.password =pass.trim();
            alert('Key Updated Successfully');
          }
        });
     }
        //keys store in localStorage
      localStorage.setItem('autonio_login_token', JSON.stringify(this.autonio_token));
  }
}
//for other exchanges
  Addkeys(exc) {
    var api= $('input[name="api"]').val();
    var sec= $('input[name="secret"]').val();
    if(!sec || !api){
      alert("Some Missing Fields, Please try again");
    }
    if (this.autonio_token.keys.length > 0) {
      $.each(this.autonio_token.keys, (i, val1) => {
        if (val1.name == exc ) {
          if(exc== 'bitshares'|| exc=='deex'){
            val1.account=api.trim();
            val1.api= sec.trim();
            alert('Key Updated Successfully');
          }
          else{
            val1.api = api.trim();
            val1.secret = sec.trim();
            alert('Key Updated Successfully');
          }
        }
      });
    }

   //keys store in localStorage
    localStorage.setItem('autonio_login_token', JSON.stringify(this.autonio_token));


  }
  //trigger on click of check updates button
  checkUpdate(){
    let current=0;
    let uname1=this.user_name;
    let value=0;
    $.ajax({
      method: 'POST',
      url: 'https://autonio.foundation/metamask_login/get_update_info.php',
    //  url: 'https://auton.io/webservices/user_check.php',
      data: { 'uname': uname1 },
      error: (data) => {
        // this.electron_er('Some Error, Seems like your Internet Connection Issue. Please try again later');
      },
      success: (data) => {
        value=parseInt(data);

        console.log("update info from backend==",data,current);
        if(value>current){
          if (window.confirm('New version is available.Press Ok to download new version for more new features,Thankyou'))
            {
              window.open('https://autonio.foundation/autonio_new/index.php#download','_blank');
            }

        }
        else if( value==current){
          alert("You are using updated version. ");
        }
      }
    });

  }
  //signout
  logout() {
    localStorage.clear();
    //console.log(localStorage.getItem('autonio_login_token'));

    this.router.navigateByUrl('/login');
  }

}
