import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import * as ti1 from 'technicalindicators';
import * as $ from 'jquery';
import * as ccxt from 'ccxt';
import * as cc from 'cryptocompare';
import { Exchange } from 'ccxt';
import * as mnt from 'moment';

@Component({
  selector: 'app-buying-settings',
  templateUrl: './buying-settings.component.html',
  styleUrls: ['./buying-settings.component.css']
})
export class BuyingSettingsComponent implements OnInit {

  autonio_token;
  algos = [];
  settings = [];

  modal_open_view: boolean = false;
  blurCss = { 'filter': 'blur(0px)' };

  constructor(public router: Router) { }

  ngOnInit() {
    if (localStorage.autonio_login_token != null) {
      this.autonio_token = JSON.parse(localStorage.getItem('autonio_login_token'));
    }
    else {
      this.autonio_token = null;
      this.router.navigateByUrl('/login');
    }

    $.ajax({
      url : 'https://autonio.foundation/webservices/get_buy_algos_user.php',
      method : 'POST',
      data : {'username' : this.autonio_token.username},
      success : (data)=>{
        this.algos = data;
      }
    });

  }

  electron_er(msg) {
    // electron.remote.showErrorBox('Error', msg);
    alert(msg);
  }

  PerCalc(a, b) {

    return parseFloat(((a / 100) * b).toFixed(8));
  }

  ModalClose() {
    this.modal_open_view = false;
    this.checkBlur();
  }

  checkBlur() {
    let style;
    if (this.modal_open_view) {
      this.blurCss = { 'filter': 'blur(5px)' };
    }
    else {
      this.blurCss = { 'filter': 'blur(0px)' };
    }
    return style;
  }

  View(settings){
    this.modal_open_view = true;
    this.checkBlur();
    settings = JSON.parse(settings);
    this.settings = settings;
    // console.log(settings);
    // $.each(settings, (i,val)=>{
    //   console.log(val);
    //   console.log(Object.keys(val));
    // });
  }

  getKey(ar){
    // console.log(ar);
    return Object.keys(ar);
  }

}
