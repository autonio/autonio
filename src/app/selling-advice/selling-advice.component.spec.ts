import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellingAdviceComponent } from './selling-advice.component';

describe('SellingAdviceComponent', () => {
  let component: SellingAdviceComponent;
  let fixture: ComponentFixture<SellingAdviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellingAdviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellingAdviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
