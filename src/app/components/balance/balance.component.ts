import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { BacktestComponent } from './../backtest/backtest.component';
import { LiveComponent } from './../live/live.component';
import { Router } from '@angular/router';
import { Http } from '@angular/http';

import * as ccxt from 'ccxt';
import * as cc from 'cryptocompare';
import * as $ from 'jquery';
import * as BitShares from 'btsdex';
import * as BncClient  from '@binance-chain/javascript-sdk';

@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.css']
})
export class BalanceComponent implements OnInit {
  public href: string = "";
  exchanges: string[];
  pairs: string[];
  bit_bal=[];
  account;
  api;
  autonio_token;
  ccxt_current;
  secretCurrent;
  apiCurrent;
  password;
  wsString = "wss://bitshares.openledger.info/ws";
  proxy = "https://cors-anywhere.herokuapp.com/";
  Difval:number;
  Desire:number;
  exchange_ar = [];
  balances=[];
  Currency;
  Balance:number;
  Usdval:number;
  sum:number;
  sellamount:number;
  buyamount:number;
  client;
  address_dex;

  constructor(private router: Router, private http: Http, private cdRef:ChangeDetectorRef) { }

ngOnInit() {

    BitShares.connect();
    this.ExchangeChange('bitfinex');
    if (localStorage.getItem('autonio_login_token') == null || localStorage.getItem('autonio_login_token') == "undefined" || localStorage.getItem('autonio_login_token') == undefined) {
      this.router.navigateByUrl('/login');
    }
    else {
      this.autonio_token = JSON.parse((localStorage.getItem('autonio_login_token')));
    }

    $.each(this.autonio_token.keys, (i, val) => {
      // if (val.api != '' && val.secret != '' &&( val.name=='bittrex' || val.name=='bitfinex')) {
      if (val.api != '' && val.secret != '') {
        this.exchange_ar.push(val.name);
      }
      else if(val.account && val.account !='' && val.name=="binance-dex"){
        this.exchange_ar.push(val.name);
      }
    });
  }
  //for rebalancing portfolio still in process
buy(val1){
  var scope=this;
  var sum=0;
    $.each(this.balances[1],function(i,val){
      sum+=val.Desire;
    });
    if(sum==100){
      $.each(this.balances[1],function(i,val){
        if(i==val1)
        {
          let buyamt=(val.Usdval/scope.sum*100)-val.Desire;

        }
      });
    }
    else{
      this.electron_er("Error:Your total portfolio percentage ("+sum+") appears to be less than 100%");
    }
    // var val=parseFloat($('input[name="percn"]').val());
    // this.desire.push(val);


  }
  //for rebalancing portfolio still in process
sell(val1){
  var scope=this;
  var sum=0;
    $.each(this.balances[1],function(i,val){
      sum+=val.Desire;
    });
    if(sum==100){
      $.each(this.balances[1],function(i,val){
        if(i==val1)
        {
          let sellamt=(val.Usdval/scope.sum*100)-val.Desire;

        }
      });
    }
    else{
      this.electron_er("Error:Your total portfolio percentage ("+sum+") appears to be less than 100%");
    }
  console.log("enter value=", val1,this.balances[1]);

}
//for rebalancing portfolio still in process
rebalance(){
    var scope=this;
    var sum=0;
    var buysell=[];
    var exc0= this.exchanges;
    var exc= exc0.toString();

    // console.log("Exchanges that is selected==",exc);
    if(exc == "binance"|| exc == "okex"|| exc == "kucoin"){
      $.each(this.balances[1],function(i,val){
        sum+=val.Desire;
      });
      if(sum==100){
        $.each(this.balances[1],function(i,val){
          val.Difval=(val.Usdval/scope.sum*100)-val.Desire;
          if(val.Difval>0){ //need to sell
            var difusdval= (val.Difval/100)*val.Usdval;//from percentage to orginal usd val
            cc.price(val.Currency, 'USD')
            .then(prices => {
              var originalval=difusdval/prices.USD; //from orginal usd val to currency val
              // console.log("amount to be sell==",originalval,difusdval,val.Difval);
              let sellary={
                'Currency':val.Currency,
                'Amount':originalval,
                'Action':'SELL',
                'Diff':val.Difval,
              };
              buysell.push(sellary);
            })
            .catch(console.error)
          }
          if(val.Difval<0){ // need to buy
            var difusdval= (val.Difval/100)*val.Usdval;//from percentage to orginal usd val
            cc.price(val.Currency, 'USD')
            .then(prices => {
              var originalval=(difusdval/prices.USD)*(-1); //from orginal usd val to currency val
              // console.log("amount to buy==",originalval,difusdval,val.Difval);
              let buyary={
                'Currency':val.Currency,
                'Amount':originalval,
                'Action':'BUY',
                'Diff':val.Difval,
              };
              buysell.push(buyary);
            })
            .catch(console.error)
          }
          // console.log("difference value==",val[i].Difval);
        });
        // console.log("array to do buy/sell==",buysell);
        this.placeorder(buysell);
      }
      else{
        this.electron_er("Error:Your total portfolio percentage ("+sum+") appears to be less than 100%");
      }
      // console.log("enter value=",this.balances[1]);
    }
    else{
      $.each(this.balances[0],function(i,val){
        sum+=val.Desire;
      });
      if(sum==100){
        $.each(this.balances[0],function(i,val){
          val.Difval=(val.Usdval/scope.sum*100)-val.Desire;
          if(val.Difval>0){ //need to sell
            var difusdval= (val.Difval/100)*val.Usdval;//from percentage to orginal usd val
            cc.price(val.Currency, 'USD')
            .then(prices => {
              var originalval=difusdval/prices.USD; //from orginal usd val to currency val
              // console.log("amount to be sell==",originalval,difusdval,val.Difval);
              let sellary={
                'Currency':val.Currency,
                'Amount':originalval,
                'Action':'SELL',
                'Diff':val.Difval,
              };
              buysell.push(sellary);
            })
            .catch(console.error)
          }
          if(val.Difval<0){ // need to buy
            var difusdval= (val.Difval/100)*val.Usdval;//from percentage to orginal usd val
            cc.price(val.Currency, 'USD')
            .then(prices => {
              var originalval=(difusdval/prices.USD)*(-1); //from orginal usd val to currency val
              // console.log("amount to buy==",originalval,difusdval,val.Difval);
              let buyary={
                'Currency':val.Currency,
                'Amount':originalval,
                'Action':'BUY',
                'Diff':val.Difval,
              };
              buysell.push(buyary);
            })
            .catch(console.error)
          }
          // console.log("difference value==",val[i].Difval);
        });
        console.log("array to do buy/sell==",buysell);
        this.placeorder(buysell);
      }
      else{
        this.electron_er("Error:Your total portfolio percentage ("+sum+") appears to be not equals to 100%");
      }
    }

  }
placeorder(buysell){
  // this.action=buysell;
  console.log("value in parameters===",buysell);
    if(buysell.Action== "SELL"){
      console.log("selling info==",buysell.Currency,buysell.Amount);
    }

}
//trigger on click of show balance button
balance(){
  var sum=0.0;
  var bal;
  var scope=this;
  var uname=this.autonio_token.username;
  let exchange = $('select[name="exchange"]').val();
  if(!exchange){
    this.electron_er('Select Exchange and Please try again.');
  }
  else{
    this.balances=[];
    $.each(this.autonio_token.keys, (i, val) => {
      if (val.name == exchange) {
        if(exchange!='bitshares' && exchange!='deex' && exchange!='binance-dex' ){
          this.apiCurrent = val.api;
          this.secretCurrent = val.secret;
          if(val.password){
            this.password=val.password;
          }
          this.ccxt_current = new ccxt[exchange];
          this.ccxt_current.apiKey=  this.apiCurrent;
          this.ccxt_current.secret =  this.secretCurrent;
          // if (!this.ccxt_current.has["CORS"] && exchange != "nebula") {
          // doesnot require for build
          // this.ccxt_current.proxy = this.proxy;
        }
        else if(exchange=='binance-dex'){
          this.account=val.account;
        }
        else{
          this.account=val.account;
          this.api=val.api;
        }
      }
    });
    // console.log("current api keys==",this.apiCurrent);
    // let exchreturn = new ccxt.exchange ({
    //       "apiKey":this.apiCurrent ,
    //       "secret": this.secretCurrent,
    //   });

      // this.ccxt_current = new ccxt[exchange];
      // this.ccxt_current.apiKey=  this.apiCurrent;
      // this.ccxt_current.secret =  this.secretCurrent;
      // if (!this.ccxt_current.has["CORS"] && exchange != "nebula") {
      // doesnot require for build
      // this.ccxt_current.proxy = this.proxy;

    if(exchange=='bittrex'){
      (async () => {
         bal =await this.ccxt_current.fetchBalance({'recvWindow': 10000000});
        // console.log("just entered to the bitfinex ====",exchange);
        // await this.ccxt_current.loadProducts();
         // bal=await this.ccxt_current.fetchBalance();
        console.log("value after fetchBalance==",bal);
      this.balances.push(bal.info);
      console.log("length of data==",this.balances[0].length);
      $.each(this.balances[0],function(i,val){
        // sum+=val.Balance;
        if(val.Balance!=0){
          cc.price(val.Currency, 'USD')
          .then(prices => {
            console.log("usd rate is ==",prices.USD,i);
            console.log("converted prices of Currency in USD==",val.Balance*prices.USD);
            this.Usdval=val.Balance*prices.USD;
            this.Desire=25;
            this.Difval=0;
            let a=val.Balance*prices.USD;
            sum+=this.Usdval;
            scope.sum=sum;
          })
          .catch(console.error)
        }

      });

    }) ()
  }
    else if(exchange=='bitfinex' || exchange=='ethfinex' ){
      (async()=>{
        // bal =await this.ccxt_current.fetchBalance({'recvWindow': 10000000});
        bal =await this.ccxt_current.fetchBalance();
        // console.log("value after fetchBalance for bitfinex==",bal.info);
        this.balances.push(bal.info);
        // console.log("value after pushing for bitfinex==",this.balances[0]);
          $.each(this.balances[0],function(i,val){
            if(val.balance!=0){
              var cur=val.currency.toUpperCase();
              cc.price(cur, 'USD')
              .then(prices => {
                console.log("usd rate is ==",prices.USD,i);
                console.log("converted prices of Currency in USD==",val.available*prices.USD);
                this.Balance=val.available;
                this.Currency=val.currency.toUpperCase();
                this.Usdval=val.available*prices.USD;
                this.Desire=25;
                this.Difval=0;
                let a=val.Balance*prices.USD;
                sum+=this.Usdval;
                scope.sum=sum;
              })
              .catch(console.error)
            }

        });
        // }
      })()
    }
    else if(exchange=='binance'|| exchange=='coss'){
      (async () => {
         bal =await this.ccxt_current.fetchBalance({'recvWindow': 10000000});
         let balnc=[];
        this.balances.push(bal.free);
        $.each(this.balances[0],function(i,val){
          if(val!=0){
            cc.price(i, 'USD')
            .then(prices => {
              let balarray= {
                'Balance':val,
                'Currency':i,
                'Usdval':val*prices.USD,
                'Desire':25,
                'Difval':0,
              };
              balnc.push(balarray);
              let a=val*prices.USD;
              sum+=a;
              scope.sum=sum;
            })
            .catch(console.error)
          }

        });
        this.balances.push(balnc);

    }) ()
    }
    else if(exchange=='okex'){
      (async () => {
         bal =await this.ccxt_current.fetchBalance();
         console.log("okex balance re===",bal.free);
         let balnc=[];
        this.balances.push(bal.free);
        $.each(this.balances[0],function(i,val){
          if(val!=0){
            cc.price(i, 'USD')
            .then(prices => {
              let balarray= {
                'Balance':val,
                'Currency':i,
                'Usdval':val*prices.USD,
                'Desire':25,
                'Difval':0,
              };
              balnc.push(balarray);
              let a=val*prices.USD;
              sum+=a;
              scope.sum=sum;
            })
            .catch(console.error)
          }

        });
        this.balances.push(balnc);
       })()
    }
    else if(exchange=='kucoin'){
      this.ccxt_current.password=this.password;
      (async () => {
         bal =await this.ccxt_current.fetchBalance();
         // console.log("kucoin balance re===",bal.free);
         let balnc=[];
        this.balances.push(bal.free);
        $.each(this.balances[0],function(i,val){
          if(val!=0){
            cc.price(i, 'USD')
            .then(prices => {
              let balarray= {
                'Balance':val,
                'Currency':i,
                'Usdval':val*prices.USD,
                'Desire':25,
                'Difval':0,
              };
              balnc.push(balarray);
              let a=val*prices.USD;
              sum+=a;
              scope.sum=sum;
            })
            .catch(console.error)
          }

        });
        this.balances.push(balnc);
        // console.log("kucion ko balances load vayesi==",this.balances[1],this.balances);
       })()
    }
    else if (exchange=='binance-dex'){
      const api = 'https://dex.binance.org/';
      this.client= new BncClient(api);
      let network="mainnet";
      this.client.chooseNetwork(network);
      var req= this.client.recoverAccountFromMnemonic(this.account);
      this.address_dex=req.address;
      // this.private_key=req.privateKey;
      // var addrecov=this.client.recoverAccountFromPrivateKey(this.private_key);
      this.bit_bal=[];
      let balnc=[
        {
          'Balance':0,
          'Currency':0,
          'Usdval':0,
          'Desire':0,
          'Difval':0
        }
      ];
      // let add="bnb1jxfh2g85q3v0tdq56fnevx6xcxtcnhtsmcu64m"
      this.getJSON("https://dex.binance.org/api/v1/account/"+this.address_dex).subscribe((data) => {
          $.each(data.balances,(i,val)=>{
            let bal_var=val.free;
            let bal_sym=val.symbol;
            if(bal_var>1){
              if(bal_sym){
                let sym=bal_sym.split('-');
                bal_sym=sym[0];
              }
              cc.price(bal_sym, 'USD')
                  .then(prices => {
                    if(prices.USD){
                      let balarray={
                        'Balance':bal_var,
                        'Currency':bal_sym,
                        'Usdval':bal_var*prices.USD,
                        'Desire':25,
                        'Difval':0,
                      };
                      balnc.push(balarray);
                      let a=bal_var*prices.USD;
                      sum+=a;
                      this.sum=sum;
                  }
                });
            }
          });
          this.bit_bal.unshift(balnc);

          console.log("array of balance==",this.bit_bal[0],this.sum,sum);

      });
    }
    else if(exchange=='bitshares'||exchange=='deex'){
      this.bit_bal=[];
      let balnc=[
        {
          'Balance':0,
          'Currency':0,
          'Usdval':0,
          'Desire':0,
          'Difval':0
        }
      ];
      BitShares.subscribe('connected',
            async () => {
                let bot = new BitShares(this.account, this.api);
                bal = await bot.balances();
                console.log("balance of bitshares==",bal);
                 $.each(bal,(i,val)=>{
                   let bal_var=val.amount;
                   let bal_cur=val.asset.symbol;
                   let pre= val.asset.precision;
                   if(bal_var>1){
                     bal_var=bal_var/(Math.pow(10,pre));
                      console.log("balance in bitshares==",bal_cur,bal_var);
                      if(bal_cur=='GDEX.ETH'){
                        cc.price('ETH', 'USD')
                           .then(prices => {
                             if(prices.USD){
                                     let balarray={
                                       'Balance':bal_var,
                                       'Currency':bal_cur,
                                       'Usdval':bal_var*prices.USD,
                                       'Desire':25,
                                       'Difval':0,
                                     };
                                     balnc.push(balarray);
                                     let a=bal_var*prices.USD;
                                     sum+=a;
                                     this.sum=sum;
                                     // console.log("sum==0=",sum,this.sum);

                                   }
                           });
                      }
                      else if(bal_cur=='GDEX.BTC'){
                        cc.price('BTC', 'USD')
                           .then(prices => {
                             if(prices.USD){
                                     let balarray={
                                       'Balance':bal_var,
                                       'Currency':bal_cur,
                                       'Usdval':bal_var*prices.USD,
                                       'Desire':25,
                                       'Difval':0,
                                     };
                                     balnc.push(balarray);
                                     let a=bal_var*prices.USD;
                                     sum+=a;
                                     this.sum=sum;
                                     // console.log("sum==1=",sum,this.sum);
                                   }
                           });
                      }
                      else if(bal_cur=='BRIDGE.PPI'){
                        cc.price('PPI', 'USD')
                           .then(prices => {
                             if(prices.USD){
                                     let balarray={
                                       'Balance':bal_var,
                                       'Currency':bal_cur,
                                       'Usdval':bal_var*prices.USD,
                                       'Desire':25,
                                       'Difval':0,
                                     };
                                     balnc.push(balarray);
                                     let a=bal_var*prices.USD;
                                     sum+=a;
                                     this.sum=sum;
                                     // console.log("sum==1=",sum,this.sum);
                                   }
                           });
                      }
                      else{
                        cc.price(bal_cur, 'USD')
                            .then(prices => {
                              if(prices.USD){
                                let balarray={
                                  'Balance':bal_var,
                                  'Currency':bal_cur,
                                  'Usdval':bal_var*prices.USD,
                                  'Desire':25,
                                  'Difval':0,
                                };
                                balnc.push(balarray);
                                let a=bal_var*prices.USD;
                                sum+=a;
                                this.sum=sum;
                                // console.log("sum==2=",sum,this.sum);
                                // console.log("Object.entries()==",Object.entries(balarray),Object.entries(balnc));
                              }
                      });

                    }

                    }
                    // this.bit_bal.push(balnc);


                 });
                 // this.bit_bal=balnc;
                 this.bit_bal.push(balnc);

                 console.log("array of balance==",this.bit_bal[0],this.sum,sum);

            }
      );
    }
  }

}

public getJSON(url) {
  return this.http.get(url).map(res => res.json());
}
electron_er(msg) {
  // electron.remote.showErrorBox('Error', msg);
  alert(msg);
}
ExchangeChange(exc) {
  if(exc!='bitshares' && exc!='deex' && exc!='binance-dex'){
    (async () => {
    let val1 = new ccxt[exc];
    //doesnot require for build
    // val1.proxy = this.proxy;
    let pair_detail = await val1.load_markets();
    this.pairs = Object.keys(pair_detail);
    this.exchanges=exc;
  })()

  }
  else{
      this.exchanges=exc;
  }
}

}
