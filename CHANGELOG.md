# Change Log
## [1.4.x and 1.5.x](https://gitlab.com/autonio/autonio/commit/f59b9195a9012ccf31560843bbf1085fb6291f3b) (2019-10-10)
- Binance API integration for Mac and Linux.
- Trading (MarketMaker) enabled for some other exchanges like BitShares, DEEX, CryptoBridge, binance-Dex,  etc.
- The algo settings in the backtest tab should now be saved locally if user performs a backtest then switches to another tab. When users return to backtest tab, the last selected settings should reopen.
- Pop-up that informs users that they need to stop the trading instance in order to be able to sell their algorithm.
- When live trade is on and user clicks on another tab, it first gives user a warning message.
- New “i” icon to all the indicators linking to the indicator information page on HelpDesk.
- New “i” icon for indicator information and minimum balance dialogue has been updated.
- Profit and profit over buy & hold are different, they are now calculated differently in the source code.
- Fixes in DEEX for asset name listed in this exchanges as:-for eg:- A2A/BEN pairings to A2A.STEX/DEEX.BITCOEN.
- Fixes in CryptoBridge exchanges for pairings /loading market-data/chart loading.
- Confusing (XBT/BTC,and others) pairings on kraken has been sorted and cleaned.
- Pairings are sorted and listed in alphabetical order.
- Indicators are sorted and listed in alphabetical order.


## [1.3.x](https://gitlab.com/autonio/autonio/commit/fe37ad4ccca8084744bc024f1aade9fbd6358205) (2019-10-03)
- Multilingual support (english and chinese).
- Trading enabled for KuCoin and Okex exchanges.
- Implemented Kucoin Open API access for third party platform.
- Check for update features.
- Market Maker implemented which can be run with centralized and decentralized exchanges.
- Market Maker Log.
- Trading view widget implemented.
- RSI upper and lower limit fixes in Backtest.


## [1.2.x](https://gitlab.com/autonio/autonio/commit/05a64e5b5e82646687d4ae61c0a4affdd181c15f) (2019-10-02)
- Pair Search option in Backtest and live Trading.
- Save algorithm features through Save My Algo button in Backtest. (By providing algorithm name and Category).
- Check saved algorithm through My Algos button in Backtest and Live Trading Component from where user can do Backtest and Live Trade with specific saved algorithm.
- Check Balance features from Live Trading Component. (for now only with some Exchanges).
- User can submit User Feedback and Enhancement Request from setting.

## [1.1.x](https://gitlab.com/autonio/autonio/commit/bae5a5905b4a14cba7e090dbc09ab73c32edaddd) (2019-09-27)
- For initial signup visit [Autonio](https://www.autonio.foundation) register with metamask.
- Trading enabled for six exchanges. They are Binance, Ethfinex, Bitfinex, Karken, Bittrex and Bitstamp along with Backtest and Livetrading.
- 20 Technical Indicators are available for trading.
- User can sell their profit making algorithm as well as can buy other's profit making algorithm.
- User can give/take Buy Sell advices.
